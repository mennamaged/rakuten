package Rakuten.TestAutomationTask.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CheckoutPage extends BasePage {

	@FindBy(xpath = "//img[@src='https://www.rakuten.de/bundle-shopcart/images/7b28df8.svg']")
	private WebElement homePageButton;

	public CheckoutPage(WebDriver driver) {
		super(driver);
	}

	public HomePage continueShopping() {
		homePageButton.click();
		return new HomePage(driver);
	}
}