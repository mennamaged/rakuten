package Rakuten.TestAutomationTask.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class BasePage {

	public static final int FIND_ELEMENT_TIMEOUT = 60; // in seconds
	public static final int POLL = 1000; // poll in milliseconds

	protected WebDriver driver;
	protected static String baseUrl;

	public static void setBaseUrl(String url) {
		baseUrl = url;
	}

	public BasePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}

	/**
	 * Wait for the page to load
	 */
	protected void waitForPageLoad() {
		new WebDriverWait(driver, FIND_ELEMENT_TIMEOUT)
				.until((ExpectedCondition<Boolean>) driver -> ((JavascriptExecutor) driver)
						.executeScript("return document.readyState").equals("complete"));
	}

	protected void waitForElementClickable(By selector) {
		WebDriverWait wait = new WebDriverWait(driver, FIND_ELEMENT_TIMEOUT, POLL);
		wait.until(ExpectedConditions.elementToBeClickable(selector));
	}

	protected void waitForElementClickable(WebElement element) {
		waitForElementClickable(element, POLL);
	}

	protected void waitForElementClickable(WebElement element, int timeout) {
		WebDriverWait wait = new WebDriverWait(driver, FIND_ELEMENT_TIMEOUT, timeout);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

}
