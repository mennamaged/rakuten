package Rakuten.TestAutomationTask.pageObjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ElektronikPage extends BasePage {

	@FindBy(xpath = "//input[@value='In den Warenkorb']")
	private WebElement checkoutButton;

	@FindBy(id = "atc_b_tsc")
	private WebElement addToCartButton;
	
	@FindBy(css = "div[class='product-img']")
	private List<WebElement> productList;

	public ElektronikPage(WebDriver driver) {
		super(driver);
	}

	public ElektronikPage selectProduct() {
		List<WebElement> li = productList;
		li.get(9).click();
		return this;
	}

	public ElektronikPage clickCheckout() {
		checkoutButton.click();
		waitForElementClickable(By.id("atc_b_tsc"));
		return this;
	}

	public CheckoutPage clickAddToCart() {
		addToCartButton.click();
		return new CheckoutPage(driver);
	}

	public ElektronikPage selectSecondProduct() {
		List<WebElement> li = productList;
		li.get(15).click();
		waitForElementClickable(By.id("rps-i-0")); // wait till the image is displayed and clickable
		return this;
	}
}
