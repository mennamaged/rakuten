package Rakuten.TestAutomationTask.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class OverviewPage extends BasePage {

	@FindBy(xpath = "//*[@class='order-container acc-overview']//*[text()='Meine letzte Bestellung']")
	private WebElement overviewLabel;

	public OverviewPage(WebDriver driver) {
		super(driver);
	}

	public String getOverviewLabelText() {
		String label = overviewLabel.getText();
		return label;
	}

}
