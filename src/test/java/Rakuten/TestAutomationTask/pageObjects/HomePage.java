package Rakuten.TestAutomationTask.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage {

	@FindBy(xpath = "//*[@class='tr_header_login']//*[text()='Einloggen']")
	private WebElement loginLink;

	@FindBy(xpath = "//span[text()='Elektronik']")
	private WebElement Elektronik;

	public HomePage(WebDriver driver) {
		super(driver);
	}

	public HomePage open(WebDriver driver) {
		driver.navigate().to(baseUrl);
		return this;
	}

	public LoginPage clickLogInLink() {
		loginLink.click();
		return new LoginPage(driver);
	}

	public ElektronikPage clickElektronik() {
		Elektronik.click();
		return new ElektronikPage(driver);
	}
}
