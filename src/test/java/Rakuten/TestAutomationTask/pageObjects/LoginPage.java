package Rakuten.TestAutomationTask.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends BasePage {

	@FindBy(xpath = "//*[@class='button-check']//*[text()='Einloggen']")
	private WebElement loginButton;

	@FindBy(xpath = "//*[@id='frame']//*[text()='Bitte überprüfen sie ihre Eingaben.']")
	private WebElement emptyFieldsErrorMessage;

	@FindBy(xpath = "//*[@class='input-container rak-form  error ']//*[text()='Die E-Mail Adresse darf nicht leer sein.']")
	private WebElement emptyEmailErrorMessage;

	@FindBy(xpath = "//input[@name='loginEmail']")
	private WebElement email;

	@FindBy(xpath = "//input[@name='loginPassword']")
	private WebElement password;

	@FindBy(xpath = "//*[@id='frame']//*[text()='Diese E-Mail-Passwort-Kombination ist uns nicht bekannt. Bitte korrigieren Sie Ihre Eingabe.']")
	private WebElement InvalidPasswordErrorMessage;

	public LoginPage(WebDriver driver) {
		super(driver);
	}

	public LoginPage clickLogInButton() {
		loginButton.click();
		return this;
	}

	public String getEmptyFieldsErrorMessageText() {
		String errorMessage = emptyFieldsErrorMessage.getText();
		return errorMessage;
	}

	public String getEmptyEmailErrorMessageText() {
		String errorMessage = emptyEmailErrorMessage.getText();
		return errorMessage;
	}

	public String getInvalidPasswordErrorMessageText() {
		String errorMessage = InvalidPasswordErrorMessage.getText();
		return errorMessage;
	}

	public OverviewPage successfulLogin() {
		email.sendKeys("rakutentesttest500@gmail.com");
		password.sendKeys("123@test.com");
		loginButton.click();
		return new OverviewPage(driver);
	}

	public LoginPage unsuccessfulLogin() {
		email.sendKeys("rakutentesttest500@gmail.com");
		password.sendKeys("123456");
		loginButton.click();
		return this;
	}

}
