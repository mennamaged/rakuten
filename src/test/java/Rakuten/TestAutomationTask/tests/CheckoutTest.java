package Rakuten.TestAutomationTask.tests;

import org.testng.annotations.Test;

import Rakuten.TestAutomationTask.pageObjects.CheckoutPage;
import Rakuten.TestAutomationTask.pageObjects.ElektronikPage;
import Rakuten.TestAutomationTask.pageObjects.HomePage;

public class CheckoutTest extends TestBase {

	@Test
	public void verifyCheckout() {
		HomePage homePageObject = new HomePage(driver);
		ElektronikPage elektronikPageObject = homePageObject.open(driver).clickElektronik().selectProduct()
				.clickCheckout();

		CheckoutPage checkooutPageObject = elektronikPageObject.clickAddToCart();
		checkooutPageObject.continueShopping();

		elektronikPageObject = homePageObject.clickElektronik().selectSecondProduct().clickCheckout();
		checkooutPageObject = elektronikPageObject.clickAddToCart();
		checkooutPageObject.continueShopping();
	}
}
