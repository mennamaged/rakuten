package Rakuten.TestAutomationTask.tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import Rakuten.TestAutomationTask.pageObjects.BasePage;

public abstract class TestBase {
	public WebDriver driver;

	@BeforeSuite
	@Parameters({ "URL" })
	public void initSuite(@Optional("https://www.rakuten.de") String URL) {
		BasePage.setBaseUrl(URL);
	}

	@BeforeMethod
	@Parameters({ "browser" })
	public void getDriverForTest(@Optional("firefox") String browser) {
		System.setProperty("webdriver.gecko.driver", "C:/Program Files/Geckodriver/geckodriver.exe");
		this.driver = new FirefoxDriver();
		this.driver.manage().window().maximize();
		this.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@AfterMethod
	public void teardown() {
		this.driver.quit();
	}

}
