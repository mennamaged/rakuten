package Rakuten.TestAutomationTask.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

import Rakuten.TestAutomationTask.pageObjects.HomePage;
import Rakuten.TestAutomationTask.pageObjects.LoginPage;
import Rakuten.TestAutomationTask.pageObjects.OverviewPage;

public class LoginTest extends TestBase {

	@Test
	public void loginWithEmptyUsernamePassword() {
		HomePage homePageObject = new HomePage(driver);
		LoginPage loginPageObject = homePageObject.open(driver).clickLogInLink();
		loginPageObject.clickLogInButton();
		Assert.assertEquals(loginPageObject.getEmptyEmailErrorMessageText(),
				"Die E-Mail Adresse darf nicht leer sein.");
	}

	@Test
	public void loginWithValidCredentials() {
		HomePage homePageObject = new HomePage(driver);
		LoginPage loginPageObject = homePageObject.open(driver).clickLogInLink();
		OverviewPage overviewPageObject = loginPageObject.successfulLogin();
		Assert.assertEquals(overviewPageObject.getOverviewLabelText(), "Meine letzte Bestellung");
	}

	@Test
	public void loginWithInvalidPassword() {
		HomePage homePageObject = new HomePage(driver);
		LoginPage loginPageObject = homePageObject.open(driver).clickLogInLink();
		loginPageObject.unsuccessfulLogin();

		Assert.assertEquals(loginPageObject.getInvalidPasswordErrorMessageText(),
				"Diese E-Mail-Passwort-Kombination ist uns nicht bekannt. Bitte korrigieren Sie Ihre Eingabe.");
	}
}
