# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Automation project using selenium webdriver with Java language and testng framework

### How do I get set up? ###

* Summary of set up:
	* Install Java
	* Use eclipse
	* Clone this project as new maven project

* Configuration:
	* First, Navigate to this class: /TestAutomationTask/src/test/java/Rakuten/TestAutomationTask/tests/TestBase.java
	* At line 27, you will need to change the path of �geckodriver.exe� according to the place of this file on your machine
	* Navigate to pom.xml and change the path of javac according to its place on your machine
	* Navigate to pom.xml >> right click >> Run as Maven Clean, then, Run as Maven install

* How to run tests:
	* You have 2 options
		* Fisrt: Open cmd in the project directory, then execute mvn clean command, mvn test
		* Second: using eclipse, right click on TestSuite.xml and Run as testng

### Project details ###

* Packages:
	* PageObjects
	* Tests

* Test Cases:
	* Login Test case to test valid and invalid scenarios for login functionality
	* Checkout Test case to add 2 products to shopping cart

* Applied concepts:
	* Page Object pattern with BasePage for page objects and TestBase for test cases
